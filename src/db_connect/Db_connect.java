package db_connect;

import java.sql.Connection;
import java.sql.DriverManager;

public class Db_connect {

        //information to connect with mysql database
    public String url = "jdbc:mysql://localhost:3306/contact_list";
    public String UID = "root";
    public String password = "";
    
    
    public static String USER;
    public static int USER_ID;
    private Connection connection;
    public void getConnected(){
        try {
            setConnection(DriverManager.getConnection(url, UID, password));
        } catch (Exception e) {
        }
    }
    
    public Connection getConnection(){
        if(connection==null){
            getConnected();
        }
        return connection;
    }
    public void setConnection(Connection connection) {
        this.connection = connection;
    }
    
}

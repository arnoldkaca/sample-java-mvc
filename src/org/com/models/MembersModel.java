package org.com.models;
import db_connect.Db_connect;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
public class MembersModel {
    Db_connect db = new Db_connect();
    ResultSet rsmem;
    public ResultSet loadMembers(){
        try {
            PreparedStatement pst;
            pst = db.getConnection().prepareStatement("select count(*) from members");
            ResultSet rs;
            rs = pst.executeQuery();
            rs.first();
            int rscount = rs.getInt(1);
            
            //get the database details
            Statement stmt = db.getConnection().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            String sql = "select * from members where status=1 order by member_id";
            rsmem = stmt.executeQuery(sql);
            rsmem.next();
        } catch (Exception e) {
            System.out.println(e);
        }
        return rsmem;
    }
    public void newMember(String name, String address, String email, String mobile_no){
        try {
            String sql = "INSERT INTO `members` (`name`, `address`, `email`, `mobile_no`, `status`, `updated_at`, `updated_by`) VALUES (?, ?, ?, ?, 1, current_timestamp, ?)";
            PreparedStatement pst = db.getConnection().prepareStatement(sql);
            pst.setString(1, name);
            pst.setString(2, address);
            pst.setString(3, email);
            pst.setString(4, mobile_no);
            pst.setInt(5, db.USER_ID);
            pst.executeUpdate();
            System.out.println("Data added to the database");
            
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void updateMember(String name, String address, String email, String mobile_no, ResultSet rs) {
        try {
            String c_email = rs.getString("email");
            String sql = "update `members` set `name`=?, `address`=?, `email`=?, `mobile_no`=?, `updated_by`=?, `updated_at`=current_timestamp where `email`=?";
            PreparedStatement pst = db.getConnection().prepareStatement(sql);
            pst.setString(1, name);
            pst.setString(2, address);
            pst.setString(3, email);
            pst.setString(4, mobile_no);
            pst.setInt(5, db.USER_ID);
            pst.setString(6, c_email);
            pst.executeUpdate();
            System.out.println("Data Changed");
            
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}

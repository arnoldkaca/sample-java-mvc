package org.com.models;
import db_connect.Db_connect;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.com.views.Login;
public class ChangePasswordModel {
    Db_connect db = new Db_connect();
    public void changePassword(String user, String oldPassword, String newPassword){
        try {
            //query the database to retreive data;
            String sql = "";
            sql = "SELECT * FROM users WHERE username=? and password=?";
            PreparedStatement pst = db.getConnection().prepareStatement(sql);
            pst.setString(1, user);
            pst.setString(2, oldPassword);
            ResultSet rs;
            rs = pst.executeQuery();
            if(rs.next()){
                //oldpassword.equals(rs.GestString("Password")) // gets the string from the column
                sql = "UPDATE users SET password=? WHERE username=?;";
                PreparedStatement pst_chp = db.getConnection().prepareStatement(sql);
                pst_chp.setString(1, newPassword);
                pst_chp.setString(2, user);
                pst_chp.executeUpdate();
                
//                String sql_changed_at = sql = "SELECT `updated_at` FROM `users` WHERE `username`=?";
//                PreparedStatement pst1 = db.getConnection().prepareStatement(sql_changed_at);
//                pst1.setString(1, user);
//                ResultSet rs1;
//                rs1 = pst1.executeQuery();
//                if(rs1.next()){
//                    System.out.println("True man validation found.");
//                }
//                String update_time = rs1.getString(1);
                JOptionPane.showMessageDialog(null, "Password changed Successfully. at: " + rs.getTimestamp(4));
                //JOptionPane.showMessageDialog(null, "Password changed Successfully. at: " + update_time);
            } else {
                JOptionPane.showMessageDialog(null, "Old Password doesn't match.");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}

package org.com.models;

import db_connect.Db_connect;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.com.views.Login;
import org.com.views.MainForm;

public class LoginModel {

    public void LoginQuery(String stru, String strp) {
        try {
            //get the database connection
            Db_connect db = new Db_connect();

            
            //query the database to retreive data;
            String sql = "";
            sql = "SELECT * FROM users WHERE username=? and password=?";
            PreparedStatement pst = db.getConnection().prepareStatement(sql);
            pst.setString(1, stru);
            pst.setString(2, strp);
            
            ResultSet rs;
            rs = pst.executeQuery();
            if(rs.next()){
                db.USER = rs.getString("username");
                db.USER_ID = rs.getInt("id");
                MainForm m = new MainForm();
                m.setVisible(true);
                //this.setVisible(false);
            } else {
                JOptionPane.showMessageDialog(null, "Username and pasword not found in the database");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}

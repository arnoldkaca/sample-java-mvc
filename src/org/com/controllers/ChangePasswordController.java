package org.com.controllers;

import javax.swing.JOptionPane;
import org.com.models.ChangePasswordModel;
import db_connect.Db_connect;

public class ChangePasswordController {
    Db_connect db = new Db_connect();
    private boolean validatePassword(String newpass){
        String pattern = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=()\\[\\]])(?=\\S+$).{8,}";
        if(!newpass.matches(pattern))
            return false;
        return true;
    }
    
    public void validate(String oldp, String newp, String confp){
        if(oldp.isEmpty() || newp.isEmpty() || confp.isEmpty()){
            JOptionPane.showMessageDialog(null, "Please make sure to fill out all the forms");
        }else if(!newp.equals(confp)){
            JOptionPane.showMessageDialog(null, "Passwords mismatch!");
        } else if(!validatePassword(newp)){
            JOptionPane.showMessageDialog(null, "Your password doesn't fullfill the requirements");
        } else {
        //hre goes the password change.
            new ChangePasswordModel().changePassword(db.USER, oldp, newp);
        }
    }
}

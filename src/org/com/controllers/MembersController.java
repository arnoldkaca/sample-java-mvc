package org.com.controllers;

import java.sql.ResultSet;
import javax.swing.JOptionPane;
import org.com.models.MembersModel;

public class MembersController {
    
    public boolean validate(String name, String Address, String email, String mobile_no){
        String[] patterns = {"(^[A-Za-z\\s]+)", "(A-Za-z0-9'\\.\\-\\s\\,)", "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@])", "(/\\(?([0-9]{3})\\)?([ .-]?)([0-9]{3})\\2([0-9]{4})/)"};
        if(!name.matches(patterns[0])){
            JOptionPane.showMessageDialog(null, "Name doesn't match to a human name.");
            return false;
        }
        if(Address.matches(patterns[1])){
            JOptionPane.showMessageDialog(null, "Address location is not correct.");
            return false;
        }
        if(email.matches(patterns[2])){
            JOptionPane.showMessageDialog(null, "E-mail is not correct.");
            return false;
        }
        if(mobile_no.matches(patterns[3])){
            JOptionPane.showMessageDialog(null, "Entered phone number is not correct.");
            return false;
        }
        return true;
    }
    
    public void newMember(String name, String Address, String email, String mobile_no){
        if(validate(name, Address, email, mobile_no)){
            new MembersModel().newMember(name, Address, email, mobile_no);
        }
    }
    
    public void updateMember (String name, String Address, String email, String mobile_no, ResultSet rs){
        if(validate(name, Address, email, mobile_no)){
            new MembersModel().updateMember(name, Address, email, mobile_no, rs);
        }
    }
    
}

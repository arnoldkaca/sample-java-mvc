package org.com.controllers;

import java.io.Console;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;
import org.com.models.LoginModel;

public class LoginController {
    
    //Create a method for validation
    private boolean validatePassword(String password){
        String pattern = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=()\\[\\]])(?=\\S+$).{8,}";
        if(!password.matches(pattern)){
            JOptionPane.showMessageDialog(null, "1. check if we have at lest one number 2. one upper case letter 3. length not less than 8 chars and last 4. search if contain at least one special character like ({}[]%$#@~&*^)");
            return false;
        }
        return true;
    }
    public void validate(String stru, String strp){
        
        validatePassword(strp);
        
        if(stru.isEmpty() || strp.isEmpty()){
            JOptionPane.showMessageDialog(null, "You should enter the username and password.");
        }else {
            LoginModel lm = new LoginModel();
            lm.LoginQuery(stru,strp);
        }
    }
}
